/**
 * @(#) Order.java
 */

public class Order
{
	private String orderNumber;
	
	private Statistics statistics;
	
	private MenuItem DishItem;
	
	private MenuItem BeverageItem;
	
	public Order(MenuItem DishItem, MenuItem BeverageItem){
		this.setDish(DishItem);
		this.setBeverage(BeverageItem);
	}
	
	public void setDish(MenuItem DishItem) {
		this.DishItem = DishItem;
	}
	
	public void setBeverage(MenuItem BeverageItem) {
		this.BeverageItem = BeverageItem;
	}
	
	public MenuItem getDish() {
		return DishItem;
	}
	
	public MenuItem getBeverage() {
		return BeverageItem;
	}

	
}
