/**
 * @(#) Person.java
 */

public abstract class Person
{
	public Person(String name) {Name = name;}
	
	private String Name;
	
	private String Surname;
	
	public String getName() {return Name;}
	
	public String getSurame() {return Surname;}
	
	public void setName(String name) {this.Name = name;}
	
	public void setSurame(String surname) {this.Surname = surname;}
}
