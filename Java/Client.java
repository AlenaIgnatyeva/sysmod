import java.util.List;
import java.util.Random;

/**
 * @(#) Client.java
 */

public class Client extends Person
{
	public Client(String name) {
		super(name);
		orders = new java.util.ArrayList<Order> ();
	}

	private String telnumber;
	
	private String taxcode;
	
	private Statistics statistics;
	
	private java.util.List<Order> orders;
	
	private String name;
	
	private String orderNumber;
	
	private MenuItem dish;
	
	private MenuItem beverage;

	
     public Order makeOrder(List<MenuItem> DishItem, List<MenuItem> BeverageItem) {
        int rnd = ((int)(Math.random() * 5));
    	dish = DishItem.get(rnd);
    	beverage = BeverageItem.get(rnd);
    	Order order = new Order(dish, beverage);
    	orders.add(order);
    	return order;
        }
     
   

	

	
	
	
	
}
