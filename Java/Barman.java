/**
 * @(#) Barman.java
 */

public class Barman extends Employee
{
	public Barman () {super("Barman", 300);}
	
	@Override
	public int getTrainingCost() {return 1200;}
		
	@Override
	public int getSatisfactionLevel() {
        if (getExperienceLevel() == ExperienceLevel.high) return 80;
        if (getExperienceLevel() == ExperienceLevel.medium) return 60;
        else return 40;
    }
	private Beverage beverage;
	
	
}
