/**
 * @(#) Experience.java
 */

public enum ExperienceLevel
{
	low, medium, high
}
