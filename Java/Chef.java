/**
 * @(#) Chef.java
 */

public class Chef extends Employee
{
	public Chef () {super("Chef", 300);}
	
	private String taxCode;
	
	@Override
	public int getTrainingCost() {return 1200;}
		
	@Override
	public int getSatisfactionLevel() {
        if (getExperienceLevel() == ExperienceLevel.high) return 80;
        if (getExperienceLevel() == ExperienceLevel.medium) return 60;
        else return 40;
    }
	
	private Dish dish;
	
	private String name;
	
	
}
