import java.util.ArrayList;
import java.util.List;

/**
 * @(#) Restaurant.java
 */

public class Restaurant
{
	private String name;
	
	private String address;
	
	private String city;
	
	private int budget;
	
	private int reputationPoints;
	
	private Quality Quality;
	
	private Reputation reputation;
	
	private int day;
	
	private java.util.List<MenuItem> DishMenu;
	
	private java.util.List<MenuItem> BeverageMenu;
	
	private java.util.List<Table> tables;
	
	private java.util.List<Order> orders;
	
	private java.util.List<Client> clients;
	
	private java.util.List<Supplier> supplier;
	
	private Menu menu;
	
	private Chef chef;
	
	private Barman barman;
	
	private java.util.List<Waiter> waiter;
	
	private java.util.List<Employee> employee;
	
	private String waiterName;
	
	private String barmanName;
	
	private String chefName;
	
	private String supplierName;
	
	public Restaurant(){
	budget = 10000;
	barman = new Barman();
    chef = new Chef();

    waiter = new java.util.ArrayList<Waiter>();
    for (int i = 0; i < 3; i++) {
        waiter.add(new Waiter("Waiter " +i));
    }

    tables = new java.util.ArrayList<Table>();
    for (int i = 0; i < 9; i++) {
        tables.add(new Table(i+1));
    }
    orders = new java.util.ArrayList<Order>();
}
	
	public void paySalary() {
		for (Employee item : employee){
			budget -= item.getSalary();
		}
	    }
	
	
	
		public void paySupplier( )
	{
		for (MenuItem item: DishMenu){
			if (item.quality == Quality.high) budget -= 10;
			else budget-=3;
		}
		for (MenuItem item: BeverageMenu){
			if (item.quality == Quality.high) budget -= 3;
			else budget-=1;
		}
	}
	
		
	public void payMonthlyCost(int cost)
	{		
		budget -= cost;
	}
	
	
	public void payForTraining( int trainingCost )
	{
		budget -= trainingCost;
			
	}
	
	public List<MenuItem> getDishMenu() {
		return DishMenu;
	}

	public void setDishMenu(List<MenuItem> dishMenu) {
		this.DishMenu = dishMenu;
	}

	public List<MenuItem> getBeverageMenu() {
		return BeverageMenu;
	}

	public void setBeverageMenu(List<MenuItem> beverageMenu) {
		this.BeverageMenu = beverageMenu;
	}
	
	public void setBudget(int budget){
		this.budget = budget;
	}
	
	public int getBudget(){
		return budget;
	}

	public Employee getChef(){
		for (Employee item: employee){
			if (item.getClass().equals(Chef.class)) return item;
		}
		return null;
	}
	
	public Employee getBarmen(){
		for (Employee item: employee){
			if (item.getClass().equals(Barman.class)) return item;
		}
		return null;
	}
	
	public void payForOrder( Order order )
	{
		budget += order.getBeverage().getPrice();
		budget += order.getDish().getPrice();
		orders.add(order);
	}

	public List<Table> getTable() {
		return tables;
	}

	public List<Employee> getEmployee() {
		// TODO Auto-generated method stub
		return employee;
	}
	
}
