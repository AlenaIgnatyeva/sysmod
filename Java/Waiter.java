import java.util.List;

/**
 * @(#) Waiter.java
 */
public class Waiter extends Employee
{
	public Waiter( String name ) {
		super(name, 200); 
		tables = new java.util.ArrayList<Table>();}
	
	private int TableNumber;
	
	private String name;
	
	private java.util.List<Table> tables;
	
	
	private Table table;
	
	public List<Table> getTables( ) {
		return tables;
	}

	public void setTables( List<Table> tables ) {
		this.tables = tables;
	}

	 public void clearTables( ){
	        if (tables == null) return;
	        for (Table table : tables) {
	            table.clear();
	            table.setWaiter(null);
	        }
	    }
	 
	@Override
	public int getTrainingCost( ) {return 800;}

	@Override
	public int getSatisfactionLevel( ) {
		if (getExperienceLevel() == ExperienceLevel.high) return 90;
        if (getExperienceLevel() == ExperienceLevel.medium) return 80;
        else return 60;
	}
	
	
}
