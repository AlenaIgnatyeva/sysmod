/**
 * @(#) Menu.java
 */

public class Menu
{
	public Menu() {
        dishes = new java.util.ArrayList<Dish>();
        beverages = new java.util.ArrayList<Beverage>();
    }
	
	private java.util.List<Dish> dishes;
	
	private java.util.List<Beverage> beverages;
	
	private MenuItem DishItem;
	
	private MenuItem BeverageItem;

}
