/**
 * @(#) Dish.java
 */

public class Dish extends MenuItem
{
	private int CalorieCount;
	
	public void setCalorieCount(Integer calorieCount) {
		CalorieCount = calorieCount;
    }

    public Integer getCalorieCount() {
        return CalorieCount;
    }
	
	
}
