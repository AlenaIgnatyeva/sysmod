/**
 * @(#) Table.java
 */

public class Table
{
	public Table(int number) {
		Number = number;
		clients = new java.util.ArrayList<Client>(2);
	}
	
	private int Number;
	
	private java.util.List<Client> clients;

	private Waiter waiter;
	
		public void clear() {
		clients.clear();
	}
		
	public void setWaiter(Waiter waiter) {
			this.waiter = waiter;
			
		}
	
	public void setNumber(int number) {
		this.Number = number;
		
	}
	public Waiter getWaiter() {
			return waiter;
		}
	
	public int getNumber() {
		return Number;
	}

	
	
}
