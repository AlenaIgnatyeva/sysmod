import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

/**
 * @(#) GameController.java
 */

public class GameController
{
	private Player player;
	
	private int day;
	
	Scanner sc = new Scanner(System.in);
	
	private Restaurant restaurant;

	private int numClient;

	private Client client;
	
	public void chooseName( String name) {
		player.setName(name);
	}
	
	public void chooseAdress( String adress) {
		player.setName(adress);
	}
	
	public Player createPlayer() {
        player = new Player(null);
        System.out.print("Enter player name");
        String name = sc.next();
        chooseName(name); 
        return player;
    }
	
	public Restaurant createRestaurant() {
        restaurant = new Restaurant();
        System.out.print("Enter restaurant name");
        String name = sc.next();
        chooseName(name); 
        System.out.print("Enter restaurant adress");
        String adress = sc.next();
        chooseAdress(adress); 
        return restaurant;
    }
	
		
	public Player startGame( )
	{
		setDishQuality(sc);
		setBeverageQuality(sc);
		System.out.print("Enter prices");
		System.out.print("Low dishes quality");
		int lowDcost = sc.nextInt();
		System.out.print("Low beverages quality");
		int lowBcost = sc.nextInt();
		System.out.print("High dishes quality");
		int highDcost = sc.nextInt();
		System.out.print("High beverages quality");
		int highBcost = sc.nextInt();
		setPrice(lowDcost, lowBcost, highDcost, highBcost);
		List<Client> client = new ArrayList<Client>();
		for (int i = 0; i < 18; i++) {
			client.add(new Client("Client"));
		}
		for (day = 1; (day <= 30) && (restaurant.getBudget() > 0); day++) {
			List<Table> table = restaurant.getTable();
			int tableCounter = 0;
			Waiter Waiter;
			assignTable(sc);
			if (table.get(tableCounter).getWaiter() == null)
				numClient = 0;
			while (numClient > 0) {
				for (int i = 0; i < 2; i++) {
					Order order = ((Client) client).makeOrder(restaurant.getDishMenu(),
					restaurant.getBeverageMenu());
			restaurant.payForOrder(order);
		}
				
			if (day % 7 == 0) {
				restaurant.paySalary();
				restaurant.paySupplier();
				System.out.print("Budget = "
						+ Integer.toString(restaurant.getBudget()));
				System.out
						.print("Do you want to increase level of worker? (1-Yes, 2 - No)");
				String answer = sc.next().toLowerCase();
				while (answer.equals("1")) {
					int numberOfEmployee = 1;
					List<Employee> employees = restaurant.getEmployee();
					for (Employee item : employees) {
						if (item.getExperienceLevel() != ExperienceLevel.high) {
							System.out.print(Integer
									.toString(numberOfEmployee)
									+ " "
									+ item.getName());
						}
						numberOfEmployee++;
					}
					System.out.print("Choose number of employee to increase level");
					int number = sc.nextInt() - 1;
					trainEmployee(employees.get(number));
					System.out.print("Increase level of someone else? (1-Yes, 2 - No)");
					answer = sc.next().toLowerCase();
				}
			}
		}
		restaurant.payMonthlyCost(4000);
		if (restaurant.getBudget() < 0)
			System.out.print("Looser!)");
		else
			System.out.print("Game over! You win!"
					+ Integer.toString(restaurant.getBudget()));
		}
				
		return player;
	}
	
	public void assignTable(Scanner sc) {
		List<Employee> employee = restaurant.getEmployee();
		List<Table> table = restaurant.getTable();
		int tableCounter = 0;
		for (Employee item : employee) {
			if (item.getClass().equals(Waiter.class)) {
				System.out.print("How many tables assign to waiter"
								+ item.getName());
				int numTables = sc.nextInt();
				while (!(numTables >= 0 && numTables <= 3)) {
					System.out.print("Choose number between 0 and 3");
					numTables = sc.nextInt();
				}
				for (int i = numTables; i > 0; i--) {
					table.get(tableCounter).setWaiter((Waiter) item);
					tableCounter++;
				}
				for (int i = tableCounter; i < table.size(); i++) {
					table.get(i).setWaiter(null);
				}
			}
		}
		}
	
	public void trainEmployee(Employee employee) {
		if (employee.getExperienceLevel() == ExperienceLevel.high) {
			System.out.print("Level is already high!");
		} else {
			if (employee.getClass().equals(Waiter.class)) {
				employee.increaseExperience();
				restaurant.payForTraining(800);
			} else {
				employee.increaseExperience();
				restaurant.payForTraining(1200);
			}
			
		}
	}
	
	public void setDishQuality(Scanner sc) {
		System.out.println("Choose number of High quality dishes");
		int hightNo = sc.nextInt();
		int lowNo = 5 - hightNo;
		List<MenuItem> dishes = new ArrayList<MenuItem>();
		for (int i = 0; i < hightNo; i++) {
			Dish dish = new Dish();
			dish.setQuality(Quality.high);
			dishes.add(dish);
		}
		for (int i = 0; i < lowNo; i++) {
			Dish dish = new Dish();
			dish.setQuality(Quality.low);
			dishes.add(dish);
		}
		restaurant.setDishMenu(dishes);
	}
	
	public void setBeverageQuality(Scanner sc) {
		System.out.println("Choose number of High quality beverages");
		int hightNo = sc.nextInt();
		int lowNo = 5 - hightNo;
		List<MenuItem> beverages = new ArrayList<MenuItem>();
		for (int i = 0; i < hightNo; i++) {
			Beverage beverage = new Beverage();
			beverage.setQuality(Quality.high);
			beverages.add(beverage);
		}
		for (int i = 0; i < lowNo; i++) {
			Beverage beverage = new Beverage();
			beverage.setQuality(Quality.low);
			beverages.add(beverage);
		}
		restaurant.setDishMenu(beverages);
	}
	
	public void setPrice(int lowDCost, int highDCost, int lowBCost,
			int highBCost) {
		for (MenuItem item : restaurant.getDishMenu()) {
			if (item.getQuality() == Quality.low)
				item.setPrice(lowDCost);
			else
				item.setPrice(highDCost);
		}
		for (MenuItem item : restaurant.getBeverageMenu()) {
			if (item.getQuality() == Quality.low)
				item.setPrice(lowBCost);
			else
				item.setPrice(highBCost);
		}
	}
	
	
}
