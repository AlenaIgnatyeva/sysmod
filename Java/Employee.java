/**
 * @(#) Employee.java
 */
public abstract class Employee extends Person
{
	private ExperienceLevel experience = ExperienceLevel.low;
	
	private int salary;
	
	private String name;
	
	public Employee(String name, int salary) {
        super(name);
        salary = salary;
    }
	
	public abstract int getTrainingCost();

	public void increaseExperience( )
	{
		if (experience == ExperienceLevel.low)
			experience = ExperienceLevel.medium;
		else if (experience == ExperienceLevel.medium)
			experience = ExperienceLevel.high;
		
	}
	
	public ExperienceLevel getExperienceLevel( ) {
        return experience;
    }
	
	public abstract int getSatisfactionLevel();
	
	public int computeSalary( )
	
	{
		int nominalSalary= salary;
		if (getExperienceLevel()  == ExperienceLevel.medium)
			nominalSalary += 100;
		else if (getExperienceLevel()  == ExperienceLevel.high)
			nominalSalary += 200;
		return nominalSalary;
	}
}
