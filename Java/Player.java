/**
 * @(#) Player.java
 */
public class Player extends Person
{
	
	public Player(String name) {
		super(name);
		
	}

	private Employee employee;
		
	private Waiter waiter;
	
	private int score;
	
	private Ranking ranking;
	
	private String name;
	
	private String employeeName;
	
	private String waiterName;
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getScore() {
		return score;
	}

	public void setScore(int score) {
		this.score = score;
	}
	
	
}
