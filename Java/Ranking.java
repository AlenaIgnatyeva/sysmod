import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * @(#) Ranking.java
 */

public class Ranking
{
	private int score;
	
	private List<Player> players;
	
	public Ranking(){
		players = new ArrayList<Player>();
	}

	public List<Player> getPlayer() {
		return players;
	}

}
