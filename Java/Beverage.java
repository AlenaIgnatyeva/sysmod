/**
 * @(#) Beverage.java
 */

public class Beverage extends MenuItem
{
	private int Volume;
	
	public void setVolume(Integer volume) {
        Volume = volume;
    }

    public Integer getVolume() {
        return Volume;
    }
	
	
}
